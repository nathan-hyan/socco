# Inmobiliarias Socco.

Landing page dedicated to a real-estate agent seller. This page was originally built with React with zero to no knowleadge. Now it's reamped and it's using NextJs for improved performance.

## Installation

1. Clone the repository to your pc
2. Install the packages with the command `npm i`
3. To run, type `npm run dev`

## Libraries used

- React
- NextJs
- FontAwesome
- Bootstrap
- SCSS

## Old versions

A copy of the old repository can be found in: [https://gitlab.com/nathan-hyan/ecommerce](https://gitlab.com/nathan-hyan/ecommerce)

## Contact

- Email: [exequiel@hyan.dev](mailto:exequiel@hyan.dev)
- Website: https://hyan.dev
- LinkedIn: [https://www.linkedin.com/in/exequielm2048/](https://www.linkedin.com/in/exequielm2048/)

> Written with [StackEdit](https://stackedit.io/).
