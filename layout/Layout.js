import NavigationBar from "./Components/NavigationBar";
import SocialBar from "./Components/SocialBar";

export default (props) => (
  <>
    <NavigationBar />
    {props.children}
    <SocialBar />
  </>
);
