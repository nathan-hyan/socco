const headerCarousel = [
  {
    file: "/img/header_carousel_1.webp",
  },
  {
    file: "/img/header_carousel_2.webp",
  },
  {
    file: "/img/header_carousel_3.webp",
  },
  {
    file: "/img/header_carousel_4.webp",
  },
];

const adCarousel = [
  {
    file: "/img/casa_display_carousel_1.webp",
  },
  {
    file: "/img/casa_display_carousel_2.webp",
  },
  {
    file: "/img/casa_display_carousel_3.webp",
  },
  {
    file: "/img/casa_display_carousel_4.webp",
  },
  {
    file: "/img/casa_display_carousel_5.webp",
  },
  {
    file: "/img/casa_display_carousel_6.webp",
  },
  {
    file: "/img/casa_display_carousel_7.webp",
  },
  {
    file: "/img/casa_display_carousel_8.webp",
  },
];

module.exports.headerCarousel = headerCarousel;
module.exports.adCarousel = adCarousel;
