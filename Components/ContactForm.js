import { Row, Col, Form, Button } from "react-bootstrap";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from 'react'


export default () => {
  const [validated, setValidated] = useState(false)

  const submit = (event) => {
    event.preventDefault();

    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    else {
      event.preventDefault();
      event.stopPropagation();

      alert('¡Gracias! Nos comunicaremos con vos a la brevedad.')
    }

    setValidated(true);
  };

  return (
    <Row
      className="d-block position-absolute bg-light rounded shadow p-3"
      style={{ zIndex: 999, top: "13%", left: "10%", width: "auto" }}
    >
      <Col>
        <h1 className="lead text-center mb-3">¡Reservá Ahora!</h1>
        <Form noValidate validated={validated} onSubmit={submit}>
          <Form.Group>
            <Form.Label>Nombre y apellido</Form.Label>
            <Form.Control type="Text" placeholder="Ingrese su nombre" required />
            <Form.Control.Feedback type='invalid'>El nombre es requerido</Form.Control.Feedback>
          </Form.Group>
          <Form.Group>
            <Form.Label>E-mail</Form.Label>
            <Form.Control type="email" placeholder="Ingrese su correo" required />
            <Form.Control.Feedback type='invalid'>Ingresa un mail con el formato <strong>nombre@dominio.com</strong></Form.Control.Feedback>
          </Form.Group>
          <Form.Group>
            <Form.Label>Teléfono</Form.Label>
            <Form.Control type="number" placeholder="Ingrese su teléfono" required />
            <Form.Control.Feedback type='invalid'>Ingresa un teléfono válido</Form.Control.Feedback>
          </Form.Group>
          <Form.Group>
            <Row>
              <Col>
                <Form.Label>Fecha de inicio</Form.Label>
                <Form.Control type="date" required />
                <Form.Control.Feedback type='invalid'>Se necesita una fecha de inicio</Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Label>Fecha de fin</Form.Label>
                <Form.Control type="date" required />
                <Form.Control.Feedback type='invalid'>Se necesita una fecha de fin</Form.Control.Feedback>
              </Col>
            </Row>
          </Form.Group>

          <Button className="w-100" variant="primary" type="submit"><FontAwesomeIcon icon={faPaperPlane} size="1x" className="mr-2 my-0 p-0" />Aplicar ahora</Button>
        </Form>
      </Col>
    </Row>
  )
};
