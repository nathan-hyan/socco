import { Navbar, Nav, Image } from "react-bootstrap";
import Link from "next/link";

export default () => (
  <Navbar collapseOnSelect expand="lg" bg="light" variant="light" fixed="top">
    <Link href="/">
      <Navbar.Brand as="a" style={{ cursor: "pointer" }}>
        <Image src="/img/logo.webp" height={25} className="mr-2 mt-n2" />
        <Image src="/img/logo-word.webp" height={25} className="mt-n2" />
      </Navbar.Brand>
    </Link>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className="mr-auto">
        <Link href="#propiedades">
          <Nav.Link as="a" style={{ cursor: "pointer" }}>
            Propiedades
          </Nav.Link>
        </Link>
        <Link href="/quienes-somos">
          <Nav.Link as="a" style={{ cursor: "pointer" }}>
            Quiénes Somos
          </Nav.Link>
        </Link>
      </Nav>
      <Nav className="ml-auto">
        <Nav.Link href="https://hyan.dev">Volver al portfolio</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
