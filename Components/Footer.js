import { Row, Col, Image } from "react-bootstrap";
import {
  faHourglass,
  faEnvelope,
  faShare,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default () => (
  <Row
    className="bg-dark text-light text-uppercase font-weight-bold mt-5 px-5"
    style={{ marginBottom: 56 }}
  >
    <Col className="py-5">
      <Image src="/img/logo-white.webp" height={52} className="mr-3 mt-n4" />
      <Image src="/img/logo-word-white.webp" height={52} className="mb-2" />
      <br />
      <a href="mailto:soccoinmo.salta@gmail.com" className="p-0 m-0 ">
        <FontAwesomeIcon icon={faEnvelope} size="1x" className="mr-2" />
        SoccoInmo.salta@gmail.com
      </a>
      <p className="p-0 m-0 text-muted ">
        <FontAwesomeIcon icon={faHourglass} size="1x" className="mr-2" />
        Lunes a Viernes de 8:30hs a 21:30hs
      </p>
    </Col>
    <Col className="py-5 text-right">
      <h1>Seguinos!</h1>
      <ul className="list-unstyled">
        <li>
          <a href="#">
            <FontAwesomeIcon icon={faShare} size="1x" className="mr-2" />
            Facebook
          </a>
        </li>
        <li>
          <a href="#">
            <FontAwesomeIcon icon={faShare} size="1x" className="mr-2" />
            Instagram
          </a>
        </li>
      </ul>
    </Col>
  </Row>
);
