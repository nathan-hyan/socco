import { Navbar, Nav } from "react-bootstrap";
import {
  faFacebookSquare,
  faWhatsappSquare,
  faInstagramSquare,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelopeSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default () => (
  <Navbar
    collapseOnSelect
    expand="lg"
    bg="primary"
    variant="dark"
    fixed="bottom"
  >
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className="mr-auto ml-auto">
        <Nav.Link
          as="a"
          target="_blank"
          href="https://www.facebook.com"
          style={{ cursor: "pointer" }}
        >
          <FontAwesomeIcon
            icon={faFacebookSquare}
            className="text-light"
            size="2x"
          />
        </Nav.Link>
        <Nav.Link
          as="a"
          target="_blank"
          href="https://www.instagram.com"
          style={{ cursor: "pointer" }}
        >
          <FontAwesomeIcon
            icon={faInstagramSquare}
            className="text-light"
            size="2x"
          />
        </Nav.Link>
        <Nav.Link
          as="a"
          target="_blank"
          href="https://wa.me/543816176275?text=Hello%20Exequiel!,%20i%27d%20like%20to%20talk%20to%20you%20about..."
          style={{ cursor: "pointer" }}
        >
          <FontAwesomeIcon
            icon={faWhatsappSquare}
            className="text-light"
            size="2x"
          />
        </Nav.Link>
        <Nav.Link
          as="a"
          target="_blank"
          href="mailto:exequiel@hyan.dev"
          style={{ cursor: "pointer" }}
        >
          <FontAwesomeIcon
            icon={faEnvelopeSquare}
            className="text-light"
            size="2x"
          />
        </Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
