import { Row, Col } from "react-bootstrap";

export default () => (
  <Row className="mx-1">
    <Col>
      <Row>
        <Col className="bg-dark text-light p-5 custom-left-bubble shadow">
          <h1 className="lead mb-3">Ramos Gabriela</h1>
          <p className="text-truncate custom-white-space">
            "
            {
              "Ante la necesidad de mi familia de conseguir un buen lugar donde habitar y ante las diversas propuestas que encontramos a través de Internet, el haber realizado el acuerdo de alquiler con la otra parte a través de la empresa Socco fue lo mejor que nos pudo pasar. \n\nFué todo rapidisimo, sin vueltas. Las asesoras nos brindaron el mejor servicio. puntualidad, seriedad y sobre todo confianza. \n\nY aún después de realizada la mudanza nos preguntan como estamos en la nueva casa. \n\nrecomiendo a esta empresa porque no nos cobraron ningún sobreprecio ni honorarios sobre el precio del alquiler."
            }
            "
          </p>
        </Col>
        <Col md="2" />
        <Col />
      </Row>
      <Row className="text-right">
        <Col />
        <Col md="2" />
        <Col className="bg-dark text-light p-5 custom-right-bubble shadow">
          <h1 className="lead mb-3">Andrea Gimenez</h1>
          <p className="text-truncate custom-white-space">
            "
            {
              "Gracias a la ayuda de consultora Socco puede y puedo administrar mejor el alquiler de mis bienes inmuebles. \n\n Sus asesoras son responsables de confianza y mediante sus requerimientos de alquiler, tanto de inquilino como yo la propietaria tenemos la seguridad de estar respaldados con seriedad y legalidad. \n\n Es recomendable sus servicios a la hora de buscar alquileres ya que no poseen tantos requisitos para alquilar permitiendo facilidades administrativas de ingreso y accesibilidad de renta y costo menor que las que hoy en dia piden las inmobiliarias."
            }
            "
          </p>
        </Col>
      </Row>
      <Row>
        <Col className="bg-dark text-light p-5 custom-left-bubble shadow">
          <h1 className="lead mb-3">Federico Díaz</h1>
          <p className="text-truncate custom-white-space">
            "
            {
              "Soy titular de deptos. Por medio de un amigo ví que la consultora realizaba servicios de alquieres y llamé.\n\nTomé las atenciones de esta empresa, me atendieron muy bien y solucionamos los problemas de alquier. \n\nRecomendado, muy responsables y eficientes."
            }
            "
          </p>
        </Col>
        <Col md="2" />
        <Col />
      </Row>
    </Col>
  </Row>
);
