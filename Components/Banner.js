export default (props) => (
  <div className="custom-banner p-5 my-3 shadow">
    <div className="custom-border border border-primary p-3">
      <h1 className="custom-banner-h1 text-center text-primary">
        {props.title}
      </h1>
      <h2 className="lead text-center custom-banner-subtitle">
        {props.subtitle}
      </h2>
    </div>
  </div>
);
