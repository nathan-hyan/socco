import CustomCarousel from "./CustomCarousel";
import { adCarousel } from "../Config/CarouselRoutes";

export default () => {
  let style = {
    borderRadius: 10,
    background: "rgba(0, 0, 0, 0.5)",
    zIndex: 999,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  };

  return (
    <div className="position-relative shadow">
      <div
        className="text-center text-light position-absolute py-3 w-75 shadow"
        style={style}
      >
        <h1 className="display-4">Tu casa %100 financiada</h1>
        <h2 className="lead">Estamos para ayudarte y cumplir tus sueños</h2>
      </div>
      <CustomCarousel
        img={adCarousel}
        height={400}
        controls={true}
        indicators={false}
      />
    </div>
  );
};
