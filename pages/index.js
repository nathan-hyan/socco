import Head from "next/head";
import Link from "next/link"

// Bootstrap components
import { Container, CardDeck } from "react-bootstrap";

// Custom components

import Layout from "../layout/Layout";
import HeaderCarousel from "../Components/HeaderCarousel";
import Banner from "../Components/Banner";
import Properties from "../Components/Properties";
import WhyChoose from "../Components/WhyChoose";
import Contact from "../Components/Contact";
import Testimonials from "../Components/Testimonials";
import Footer from "../Components/Footer";
import AdBanner from "../Components/AdBanner";

// Config
import { firstRow, secondRow } from "../Config/CardsRoutes";

export default function Home() {
  return (
    <Container fluid>
      <Head>
        <title>Socco</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Layout>
        <main style={{ marginTop: 56 }}>
          <HeaderCarousel />
          <Banner title="¡Últimas propiedades en promoción!" subtitle="" />
          <CardDeck id="propiedades">
            {firstRow.map((item, index) => (
              <Properties
                key={index}
                title={item.title}
                subtitle={item.subtitle}
                body={item.body}
                img={item.img}
              />
            ))}
          </CardDeck>
          <Banner
            title="Accedé hoy a tu casa prefabricada financiada 100%"
            subtitle="¡Anticipo de $5000 saldo a 24 cuotas fijas y en pesos!"
          />
          <CardDeck>
            {secondRow.map((item, index) => (
              <Properties
                key={index}
                title={item.title}
                subtitle={item.subtitle}
                body={item.body}
                img={item.img}
              />
            ))}
          </CardDeck>
          <WhyChoose />
          <AdBanner />
          <Contact />
          <Testimonials />
        </main>
      </Layout>

      <footer>
        <Footer />
      </footer>
    </Container>
  );
}
