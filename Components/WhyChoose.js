import { Row, Col } from "react-bootstrap";
import {
  faClipboard,
  faLayerGroup,
  faMapMarkedAlt,
  faThumbsUp,
  faMapMarkerAlt,
  faPercent,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default () => (
  <Row className="my-3 text-center">
    <Col>
      <Row className="mb-3">
        <Col>
          <h1 className="display-4">
            ¿Por qué elegir <strong className="text-primary">Socco</strong>?
          </h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <Row>
            <Col>
              <Row className="mb-3">
                <Col>
                  <FontAwesomeIcon
                    icon={faClipboard}
                    size="3x"
                    className="text-primary"
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h2 className="custom-h2">Registro gratis sin comisiones</h2>
                  <p className="custom-p">
                    De día o de noche, estamos aquí para lo que necesites. Habla
                    con nuestro equipo de atención al cliente a cualquier hora,
                    desde cualquier lugar del mundo.
                  </p>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="mb-3">
                <Col>
                  <FontAwesomeIcon
                    icon={faLayerGroup}
                    size="3x"
                    className="text-primary"
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h2 className="custom-h2">
                    Valoración de la propiedad sin cargo
                  </h2>
                  <p className="custom-p">
                    Para garantizar tu seguridad, la de tu alojamiento y tus
                    pertenencias, cubrimos todas las reservaciones con una
                    protección de daños a la propiedad y un seguro contra
                    accidentes con valor de 100.000 mil pesos.
                  </p>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="mb-3">
                <Col>
                  <FontAwesomeIcon
                    icon={faMapMarkedAlt}
                    size="3x"
                    className="text-primary"
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h2 className="custom-h2">
                    Cobertura total en 7 oficinas, Múltiples ubicaciones
                  </h2>
                  <p className="custom-p">
                    Contamos con excelentes ubicaciones en las provincias de
                    Buenos Aires, Córdoba, Santiago del Estero,Tucumán, Salta y
                    Jujuy
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col>
              <Row className="mb-3">
                <Col>
                  <FontAwesomeIcon
                    icon={faThumbsUp}
                    size="3x"
                    className="text-primary"
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h2 className="custom-h2">Tus propiedades son tus reglas</h2>
                  <p className="custom-p">
                    Para ayudar a establecer expectativas, puedes agregar tus
                    Reglas de la casa que los huéspedes deben aceptar antes de
                    reservar tu alojamiento, por ejemplo, restringir el cigarro
                    y los eventos. Si un huésped incumple una de estas reglas
                    una vez que haya reservado
                  </p>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="mb-3">
                <Col>
                  <FontAwesomeIcon
                    icon={faMapMarkerAlt}
                    size="3x"
                    className="text-primary"
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h2 className="custom-h2">Evaluaciones bilaterales justas</h2>
                  <p className="custom-p">
                    Para ayudar a fortalecer la confianza y la reputación en
                    Socco, los huéspedes y propietarios se evalúan mutuamente
                    después de completar cada reservación. Antes de alojar a un
                    huésped, puedes echarle un vistazo a sus evaluaciónes de
                    otros propietarios.
                  </p>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="mb-3">
                <Col>
                  <FontAwesomeIcon
                    icon={faPercent}
                    size="3x"
                    className="text-primary"
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <h2 className="custom-h2">¿Por qué hospedar en Socco?</h2>
                  <p className="custom-p">
                    No importa qué tipo de alojamiento o habitación tengas para
                    compartir, alojar huéspedes con Socco resulta sencillo y
                    seguro. Tienes control total de tu disponibilidad, precios,
                    reglas de la casa y de cómo interactuás con los huéspedes.{" "}
                  </p>
                </Col>
              </Row>
            </Col>{" "}
          </Row>
        </Col>
      </Row>
    </Col>
  </Row>
);
