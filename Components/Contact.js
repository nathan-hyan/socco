import { Form, Button, Col, Row } from "react-bootstrap";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from 'react'


export default () => {
  const [validated, setValidated] = useState(false)

  const submit = (event) => {
    event.preventDefault();

    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    
    else {
      event.preventDefault();
      event.stopPropagation();

      alert('¡Gracias! Nos comunicaremos con vos a la brevedad.')
    }

    setValidated(true);
  };

  return (
    <div className="my-5 p-5 shadow custom-contact">
      <h1 className="display-4 border-bottom border-primary d-inline custom-border">
        Contactate con un asesor
    </h1>
      <Form noValidate validated={validated} className="mt-5" onSubmit={submit}>
        <Row>
          <Col>
            <Form.Group controlId="contactEmail">
              <Form.Label>Dirección de correo:</Form.Label>
              <Form.Control required type="email" placeholder="Ingrese el correo" />
              <Form.Control.Feedback type='invalid'>Ingresa un mail con el formato <strong>nombre@dominio.com</strong></Form.Control.Feedback>
              <Form.Text className="text-muted">Nos comunicaremos a través de este medio.</Form.Text>
            </Form.Group>

            <Form.Group controlId="contactName">
              <Form.Label>Nombre</Form.Label>
              <Form.Control type="Name" placeholder="Su nombre" required />
              <Form.Control.Feedback type='invalid'>El nombre es requerido</Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="contactTextArea">
              <Form.Label>Mensaje</Form.Label>
              <Form.Control as="textarea" rows="3" required />
              <Form.Control.Feedback type='invalid'>Por favor, ingresá un mensaje.</Form.Control.Feedback>
            </Form.Group>
            <Button variant="primary" type="submit">
              <FontAwesomeIcon
                icon={faPaperPlane}
                size="1x"
                className="mr-2 my-0 p-0"
              />
            Enviar
          </Button>
          </Col>
          <Col md="6" />
        </Row>
      </Form>
    </div>
  )
};
