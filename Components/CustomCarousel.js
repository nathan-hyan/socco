import { Carousel } from "react-bootstrap";

export default (props) => (
  <Carousel
    controls={props.controls}
    indicators={props.indicators}
    className={props.className}
  >
    {props.img.map((item, index) => {
      return (
        <Carousel.Item key={index} style={{ height: props.height }}>
          <div
            className="h-100 w-100"
            style={{
              background: `url(${item.file}) no-repeat center`,
              backgroundSize: "cover",
            }}
          />
        </Carousel.Item>
      );
    })}
  </Carousel>
);
