import CustomCarousel from "./CustomCarousel";
import { headerCarousel } from "../Config/CarouselRoutes";
import ContactForm from "./ContactForm";

export default () => (
  <div className="shadow">
    <ContactForm className="custom-contact-form position-absolute" />
    <CustomCarousel
      img={headerCarousel}
      height={500}
      controls={false}
      indicators={false}
    />
  </div>
);
