const firstRow = [
  {
    title: "Monoambiente 🏬",
    subtitle: "Temporal",
    body:
      "✅ Hermosa vista\n✅ 4 Camas simples\n✅ Cochera\n✅ Totalmente amoblado\n✅ Zona Centro",
    img: [
      {
        file: "/img/casa_display_1a.webp",
      },
      {
        file: "/img/casa_display_1b.webp",
      },
      {
        file: "/img/casa_display_1c.webp",
      },
    ],
  },
  {
    title: "Casa 🏠",
    subtitle: "Alquiler",
    body:
      "✅ Ubicada en la Calle Balcarce\n✅ Living-comedor\n✅ Cocina-comedor\n✅ Patio y parrilla\n✅ Semi amoblada.",
    img: [
      {
        file: "/img/casa_display_2a.webp",
      },
      {
        file: "/img/casa_display_2b.webp",
      },
      {
        file: "/img/casa_display_2c.webp",
      },
    ],
  },
  {
    title: "Departamento Ecológico 🌱",
    subtitle: "Temporal",
    body:
      "✅ Zona Shoping.\n✅ Totalmente Amoblado\n✅ Capacidad para 3 personas.\n✅ Hermosa vista",
    img: [
      {
        file: "/img/casa_display_3a.webp",
      },
      {
        file: "/img/casa_display_3b.webp",
      },
      {
        file: "/img/casa_display_3c.webp",
      },
      {
        file: "/img/casa_display_3d.webp",
      },
      {
        file: "/img/casa_display_3e.webp",
      },
    ],
  },
  {
    title: "Departamento 🏬",
    subtitle: "Alquiler",
    body:
      "✅ Zona Centro\n✅ Balcón con Parrillero\n✅ Hermosa vista\n✅ Quincho\n✅ Solárium",
    img: [
      {
        file: "/img/casa_display_4a.webp",
      },
      {
        file: "/img/casa_display_4c.webp",
      },
      {
        file: "/img/casa_display_4d.webp",
      },
      {
        file: "/img/casa_display_4e.webp",
      },
      {
        file: "/img/casa_display_4f.webp",
      },
    ],
  },
];
const secondRow = [
  {
    title: "Pre-moldeadas",
    subtitle: "Casas",
    body:
      '✅ Paredes & Muros\nNuestras paredes están compuestas por paneles estructurales de madera de 1". Su parte exterior se recubre con placas de fibrocemento. Y en su interior se reviste Durlock.\n\n✅ Techo\nLa cubierta superior está compuesta por un cielo un cielo raso de machimbre de pino de primera calidady en su exterior se  recubre de chapa de cinc calibre 30.\n\n✅ Aberturas\nLas ventanas son de aluminio blanco tipo corrediza con vidrios colocados de 1,20 x 1,50, las ventanas cocineras y de baños son de aluminio blanco con vidrios colocados y las medidas de las mismas son según las distintas tipologías.\nLa puerta de frente es de doble chapa inyectada con manija fija y picaporte interior.',
    img: [
      {
        file: "/img/prefab_card_1a.webp",
      },
      {
        file: "/img/prefab_card_1b.webp",
      },
    ],
  },
  {
    title: "Unidad clásica",
    subtitle: "Casas Pre-fabricadas",
    body:
      '✅ Paredes & Muros\nNuestras paredes están compuestas por paneles estructurales de madera de pino misionero de 1", la madera esta tratada contra plagas y humedad.\n\n✅ Techo\nPosee una cubierta exterior de chapa de cinc calibre 30 en dos aguas.\n\n✅ Aberturas\nPosee puertas y ventanas de madera con sus respectivos marcos y bisagras.',
    img: [
      {
        file: "/img/prefab_card_2a.webp",
      },
      {
        file: "/img/prefab_card_2b.webp",
      },
    ],
  },
  {
    title: "Unidad cabaña",
    subtitle: "Casas Pre-fabricadas",
    body:
      '✅ Paredes & Muros\nNuestras paredes están compuestas por paneles estructurales de madera de pino misionero de 1,5" y posee doble revestimiento, nuestras maderas están tratadas contra plagas y humedad.\n\n✅ Techo\nPosee un cielorraso de madera de pino misionero cubierta exterior de chapa de cinc calibre 30.\n\n✅ Aberturas\nPosee puertas y ventanas de madera.',
    img: [
      {
        file: "/img/prefab_card_3a.webp",
      },
      {
        file: "/img/prefab_card_3b.webp",
      },
      {
        file: "/img/prefab_card_3c.webp",
      },
    ],
  },
];

module.exports.firstRow = firstRow;
module.exports.secondRow = secondRow;
