import { Card, Button } from "react-bootstrap";
import CustomCarousel from "./CustomCarousel";
import { faEye, faPhone } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default (props) => {
  return (
    <Card style={{ width: "18rem" }} className="shadow">
      <CustomCarousel
        img={props.img}
        height={180}
        controls={true}
        indicators={false}
      />
      <Card.Body style={{ height: 230, overflow: "hidden" }}>
        <Card.Subtitle className="mb-2 text-muted text-uppercase">
          {props.subtitle}
        </Card.Subtitle>
        <Card.Title>{props.title}</Card.Title>
        <Card.Text className="mt-3 custom-white-space">{props.body}</Card.Text>
      </Card.Body>
      <Card.Footer>
        <Button variant="primary" className="mr-3">
          <p className="m-0 p-0">
            <FontAwesomeIcon icon={faEye} size="1x" className="mr-2 my-0 p-0" />
            Ver más
          </p>
        </Button>
        <Button variant="primary">
          <p className="m-0 p-0">
            <FontAwesomeIcon
              icon={faPhone}
              size="1x"
              className="mr-2 my-0 p-0"
            />
            Contactarse
          </p>{" "}
        </Button>
      </Card.Footer>
    </Card>
  );
};
